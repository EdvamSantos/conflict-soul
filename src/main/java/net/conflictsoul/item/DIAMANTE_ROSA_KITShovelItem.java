
package net.conflictsoul.item;

import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.ShovelItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Item;

import net.conflictsoul.init.ConflictSoulModItems;

public class DIAMANTE_ROSA_KITShovelItem extends ShovelItem {
	public DIAMANTE_ROSA_KITShovelItem() {
		super(new Tier() {
			public int getUses() {
				return 323;
			}

			public float getSpeed() {
				return 7f;
			}

			public float getAttackDamageBonus() {
				return 0f;
			}

			public int getLevel() {
				return 2;
			}

			public int getEnchantmentValue() {
				return 17;
			}

			public Ingredient getRepairIngredient() {
				return Ingredient.of(new ItemStack(ConflictSoulModItems.MINERIO_DIAMANTE_ROSA.get()));
			}
		}, 1, -3f, new Item.Properties());
	}
}
