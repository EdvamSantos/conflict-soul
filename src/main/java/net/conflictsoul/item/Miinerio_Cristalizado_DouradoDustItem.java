
package net.conflictsoul.item;

import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.Item;

public class Miinerio_Cristalizado_DouradoDustItem extends Item {
	public Miinerio_Cristalizado_DouradoDustItem() {
		super(new Item.Properties().stacksTo(64).rarity(Rarity.COMMON));
	}
}
