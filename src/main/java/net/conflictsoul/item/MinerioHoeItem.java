
package net.conflictsoul.item;

import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.HoeItem;

import net.conflictsoul.init.ConflictSoulModItems;

public class MinerioHoeItem extends HoeItem {
	public MinerioHoeItem() {
		super(new Tier() {
			public int getUses() {
				return 250;
			}

			public float getSpeed() {
				return 6f;
			}

			public float getAttackDamageBonus() {
				return 0f;
			}

			public int getLevel() {
				return 2;
			}

			public int getEnchantmentValue() {
				return 14;
			}

			public Ingredient getRepairIngredient() {
				return Ingredient.of(new ItemStack(ConflictSoulModItems.MINERIO_DO_MINEIRO_INGOT.get()));
			}
		}, 0, -3f, new Item.Properties());
	}
}
