
package net.conflictsoul.item;

import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.Item;

public class MinerioDeCactoItem extends Item {
	public MinerioDeCactoItem() {
		super(new Item.Properties().stacksTo(64).rarity(Rarity.COMMON));
	}
}
