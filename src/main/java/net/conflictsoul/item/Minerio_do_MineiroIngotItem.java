
package net.conflictsoul.item;

import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.Item;

public class Minerio_do_MineiroIngotItem extends Item {
	public Minerio_do_MineiroIngotItem() {
		super(new Item.Properties().stacksTo(64).rarity(Rarity.COMMON));
	}
}
