
package net.conflictsoul.item;

import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.PickaxeItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Item;

import net.conflictsoul.init.ConflictSoulModItems;

public class MinerioPickaxeItem extends PickaxeItem {
	public MinerioPickaxeItem() {
		super(new Tier() {
			public int getUses() {
				return 250;
			}

			public float getSpeed() {
				return 6f;
			}

			public float getAttackDamageBonus() {
				return 0f;
			}

			public int getLevel() {
				return 2;
			}

			public int getEnchantmentValue() {
				return 14;
			}

			public Ingredient getRepairIngredient() {
				return Ingredient.of(new ItemStack(ConflictSoulModItems.MINERIO_DO_MINEIRO_INGOT.get()));
			}
		}, 1, -3f, new Item.Properties());
	}
}
