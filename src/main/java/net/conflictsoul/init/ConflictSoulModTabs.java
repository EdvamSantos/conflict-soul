
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.conflictsoul.init;

import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.BuildCreativeModeTabContentsEvent;

import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.core.registries.Registries;

import net.conflictsoul.ConflictSoulMod;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ConflictSoulModTabs {
	public static final DeferredRegister<CreativeModeTab> REGISTRY = DeferredRegister.create(Registries.CREATIVE_MODE_TAB, ConflictSoulMod.MODID);

	@SubscribeEvent
	public static void buildTabContentsVanilla(BuildCreativeModeTabContentsEvent tabData) {

		if (tabData.getTabKey() == CreativeModeTabs.BUILDING_BLOCKS) {
			tabData.accept(ConflictSoulModBlocks.MADEIRA_CLARA_ROSADA_STAIRS.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_CLARA_ROSADA_SLAB.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_CLARA_ROSADA_FENCE.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_CLARA_ROSADA_FENCE_GATE.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_AZUL_FORTE_STAIRS.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_AZUL_FORTE_SLAB.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_AZUL_FORTE_BUTTON.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_BRANCA_WOOD.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_BRANCA_LOG.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_BRANCA_PLANKS.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_BRANCA_STAIRS.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_BRANCA_BUTTON.get().asItem());
		}

		if (tabData.getTabKey() == CreativeModeTabs.REDSTONE_BLOCKS) {
			tabData.accept(ConflictSoulModBlocks.MADEIRA_CLARA_ROSADA_PRESSURE_PLATE.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_AZUL_FORTE_FENCE_GATE.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_AZUL_FORTE_PRESSURE_PLATE.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_BRANCA_FENCE_GATE.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_BRANCA_PRESSURE_PLATE.get().asItem());
		}

		if (tabData.getTabKey() == CreativeModeTabs.COMBAT) {
			tabData.accept(ConflictSoulModItems.A_SWORD.get());
			tabData.accept(ConflictSoulModItems.DIAMANTE_ROSA_KIT_SWORD.get());
			tabData.accept(ConflictSoulModItems.MINERIO_SWORD.get());
			tabData.accept(ConflictSoulModItems.CRISTALI_SWORD.get());
		}

		if (tabData.getTabKey() == CreativeModeTabs.INGREDIENTS) {
			tabData.accept(ConflictSoulModBlocks.MINERIO_DO_MINEIRO_BLOCK.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MINERIO_DO_MINEIRO_ORE.get().asItem());
			tabData.accept(ConflictSoulModItems.MINERIO_DO_MINEIRO_INGOT.get());
			tabData.accept(ConflictSoulModBlocks.MINERIO_DIAMANTE_ROSA_ORE.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MINERIO_DIAMANTE_ROSA_BLOCK.get().asItem());
			tabData.accept(ConflictSoulModItems.MINERIO_DIAMANTE_ROSA.get());
			tabData.accept(ConflictSoulModBlocks.MINERIO_DE_CACTO_ORE.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MINERIO_DE_CACTO_BLOCK.get().asItem());
			tabData.accept(ConflictSoulModItems.MINERIO_DE_CACTO.get());
			tabData.accept(ConflictSoulModBlocks.MIINERIO_CRISTALIZADO_DOURADO_ORE.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MIINERIO_CRISTALIZADO_DOURADO_BLOCK.get().asItem());
			tabData.accept(ConflictSoulModItems.MIINERIO_CRISTALIZADO_DOURADO_DUST.get());
		}

		if (tabData.getTabKey() == CreativeModeTabs.TOOLS_AND_UTILITIES) {
			tabData.accept(ConflictSoulModItems.A_AXE.get());
			tabData.accept(ConflictSoulModItems.A_PICKAXE.get());
			tabData.accept(ConflictSoulModItems.A_SHOVEL.get());
			tabData.accept(ConflictSoulModItems.A_HOE.get());
			tabData.accept(ConflictSoulModItems.DIAMANTE_ROSA_KIT_PICKAXE.get());
			tabData.accept(ConflictSoulModItems.DIAMANTE_ROSA_KIT_AXE.get());
			tabData.accept(ConflictSoulModItems.DIAMANTE_ROSA_KIT_SHOVEL.get());
			tabData.accept(ConflictSoulModItems.DIAMANTE_ROSA_KIT_HOE.get());
			tabData.accept(ConflictSoulModItems.MINERIO_PICKAXE.get());
			tabData.accept(ConflictSoulModItems.MINERIO_AXE.get());
			tabData.accept(ConflictSoulModItems.MINERIO_SHOVEL.get());
			tabData.accept(ConflictSoulModItems.MINERIO_HOE.get());
			tabData.accept(ConflictSoulModItems.CRISTALI_PICKAXE.get());
			tabData.accept(ConflictSoulModItems.CRISTALI_AXE.get());
			tabData.accept(ConflictSoulModItems.CRISTALI_SHOVEL.get());
			tabData.accept(ConflictSoulModItems.CRISTALI_HOE.get());
		}

		if (tabData.getTabKey() == CreativeModeTabs.NATURAL_BLOCKS) {
			tabData.accept(ConflictSoulModBlocks.MADEIRA_CLARA_ROSADA_WOOD.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_CLARA_ROSADA_LOG.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_CLARA_ROSADA_PLANKS.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_CLARA_ROSADA_LEAVES.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_AZUL_FORTE_WOOD.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_AZUL_FORTE_PLANKS.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_AZUL_FORTE_LOG.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_AZUL_FORTE_LEAVES.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_AZUL_FORTE_FENCE.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_BRANCA_LEAVES.get().asItem());
			tabData.accept(ConflictSoulModBlocks.MADEIRA_BRANCA_FENCE.get().asItem());
		}
	}
}
