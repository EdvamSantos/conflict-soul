
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.conflictsoul.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.common.Mod;

import net.minecraft.world.level.levelgen.feature.Feature;

import net.conflictsoul.world.features.ores.Minerio_do_MineiroOreFeature;
import net.conflictsoul.world.features.ores.MinerioDiamanteRosaOreFeature;
import net.conflictsoul.world.features.ores.MinerioDeCactoOreFeature;
import net.conflictsoul.world.features.ores.Miinerio_Cristalizado_DouradoOreFeature;
import net.conflictsoul.world.features.ores.MADEIRA_CLARA_ROSADALogFeature;
import net.conflictsoul.world.features.ores.MADEIRA_CLARA_ROSADALeavesFeature;
import net.conflictsoul.world.features.ores.MADEIRA_BRANCALogFeature;
import net.conflictsoul.world.features.ores.MADEIRA_BRANCALeavesFeature;
import net.conflictsoul.world.features.ores.MADEIRA_AZUL_FORTELogFeature;
import net.conflictsoul.world.features.ores.MADEIRA_AZUL_FORTELeavesFeature;
import net.conflictsoul.ConflictSoulMod;

@Mod.EventBusSubscriber
public class ConflictSoulModFeatures {
	public static final DeferredRegister<Feature<?>> REGISTRY = DeferredRegister.create(ForgeRegistries.FEATURES, ConflictSoulMod.MODID);
	public static final RegistryObject<Feature<?>> MINERIO_DO_MINEIRO_ORE = REGISTRY.register("minerio_do_mineiro_ore", Minerio_do_MineiroOreFeature::new);
	public static final RegistryObject<Feature<?>> MINERIO_DIAMANTE_ROSA_ORE = REGISTRY.register("minerio_diamante_rosa_ore", MinerioDiamanteRosaOreFeature::new);
	public static final RegistryObject<Feature<?>> MINERIO_DE_CACTO_ORE = REGISTRY.register("minerio_de_cacto_ore", MinerioDeCactoOreFeature::new);
	public static final RegistryObject<Feature<?>> MIINERIO_CRISTALIZADO_DOURADO_ORE = REGISTRY.register("miinerio_cristalizado_dourado_ore", Miinerio_Cristalizado_DouradoOreFeature::new);
	public static final RegistryObject<Feature<?>> MADEIRA_CLARA_ROSADA_LOG = REGISTRY.register("madeira_clara_rosada_log", MADEIRA_CLARA_ROSADALogFeature::new);
	public static final RegistryObject<Feature<?>> MADEIRA_CLARA_ROSADA_LEAVES = REGISTRY.register("madeira_clara_rosada_leaves", MADEIRA_CLARA_ROSADALeavesFeature::new);
	public static final RegistryObject<Feature<?>> MADEIRA_AZUL_FORTE_LOG = REGISTRY.register("madeira_azul_forte_log", MADEIRA_AZUL_FORTELogFeature::new);
	public static final RegistryObject<Feature<?>> MADEIRA_AZUL_FORTE_LEAVES = REGISTRY.register("madeira_azul_forte_leaves", MADEIRA_AZUL_FORTELeavesFeature::new);
	public static final RegistryObject<Feature<?>> MADEIRA_BRANCA_LOG = REGISTRY.register("madeira_branca_log", MADEIRA_BRANCALogFeature::new);
	public static final RegistryObject<Feature<?>> MADEIRA_BRANCA_LEAVES = REGISTRY.register("madeira_branca_leaves", MADEIRA_BRANCALeavesFeature::new);
}
