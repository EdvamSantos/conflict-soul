
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.conflictsoul.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.Block;

import net.conflictsoul.block.Minerio_do_MineiroOreBlock;
import net.conflictsoul.block.Minerio_do_MineiroBlockBlock;
import net.conflictsoul.block.MinerioDiamanteRosaOreBlock;
import net.conflictsoul.block.MinerioDiamanteRosaBlockBlock;
import net.conflictsoul.block.MinerioDeCactoOreBlock;
import net.conflictsoul.block.MinerioDeCactoBlockBlock;
import net.conflictsoul.block.Miinerio_Cristalizado_DouradoOreBlock;
import net.conflictsoul.block.Miinerio_Cristalizado_DouradoBlockBlock;
import net.conflictsoul.block.MADEIRA_CLARA_ROSADAWoodBlock;
import net.conflictsoul.block.MADEIRA_CLARA_ROSADAStairsBlock;
import net.conflictsoul.block.MADEIRA_CLARA_ROSADASlabBlock;
import net.conflictsoul.block.MADEIRA_CLARA_ROSADAPressurePlateBlock;
import net.conflictsoul.block.MADEIRA_CLARA_ROSADAPlanksBlock;
import net.conflictsoul.block.MADEIRA_CLARA_ROSADALogBlock;
import net.conflictsoul.block.MADEIRA_CLARA_ROSADALeavesBlock;
import net.conflictsoul.block.MADEIRA_CLARA_ROSADAFenceGateBlock;
import net.conflictsoul.block.MADEIRA_CLARA_ROSADAFenceBlock;
import net.conflictsoul.block.MADEIRA_CLARA_ROSADAButtonBlock;
import net.conflictsoul.block.MADEIRA_BRANCAWoodBlock;
import net.conflictsoul.block.MADEIRA_BRANCAStairsBlock;
import net.conflictsoul.block.MADEIRA_BRANCASlabBlock;
import net.conflictsoul.block.MADEIRA_BRANCAPressurePlateBlock;
import net.conflictsoul.block.MADEIRA_BRANCAPlanksBlock;
import net.conflictsoul.block.MADEIRA_BRANCALogBlock;
import net.conflictsoul.block.MADEIRA_BRANCALeavesBlock;
import net.conflictsoul.block.MADEIRA_BRANCAFenceGateBlock;
import net.conflictsoul.block.MADEIRA_BRANCAFenceBlock;
import net.conflictsoul.block.MADEIRA_BRANCAButtonBlock;
import net.conflictsoul.block.MADEIRA_AZUL_FORTEWoodBlock;
import net.conflictsoul.block.MADEIRA_AZUL_FORTEStairsBlock;
import net.conflictsoul.block.MADEIRA_AZUL_FORTESlabBlock;
import net.conflictsoul.block.MADEIRA_AZUL_FORTEPressurePlateBlock;
import net.conflictsoul.block.MADEIRA_AZUL_FORTEPlanksBlock;
import net.conflictsoul.block.MADEIRA_AZUL_FORTELogBlock;
import net.conflictsoul.block.MADEIRA_AZUL_FORTELeavesBlock;
import net.conflictsoul.block.MADEIRA_AZUL_FORTEFenceGateBlock;
import net.conflictsoul.block.MADEIRA_AZUL_FORTEFenceBlock;
import net.conflictsoul.block.MADEIRA_AZUL_FORTEButtonBlock;
import net.conflictsoul.ConflictSoulMod;

public class ConflictSoulModBlocks {
	public static final DeferredRegister<Block> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCKS, ConflictSoulMod.MODID);
	public static final RegistryObject<Block> MINERIO_DO_MINEIRO_BLOCK = REGISTRY.register("minerio_do_mineiro_block", () -> new Minerio_do_MineiroBlockBlock());
	public static final RegistryObject<Block> MINERIO_DO_MINEIRO_ORE = REGISTRY.register("minerio_do_mineiro_ore", () -> new Minerio_do_MineiroOreBlock());
	public static final RegistryObject<Block> MINERIO_DIAMANTE_ROSA_ORE = REGISTRY.register("minerio_diamante_rosa_ore", () -> new MinerioDiamanteRosaOreBlock());
	public static final RegistryObject<Block> MINERIO_DIAMANTE_ROSA_BLOCK = REGISTRY.register("minerio_diamante_rosa_block", () -> new MinerioDiamanteRosaBlockBlock());
	public static final RegistryObject<Block> MINERIO_DE_CACTO_ORE = REGISTRY.register("minerio_de_cacto_ore", () -> new MinerioDeCactoOreBlock());
	public static final RegistryObject<Block> MINERIO_DE_CACTO_BLOCK = REGISTRY.register("minerio_de_cacto_block", () -> new MinerioDeCactoBlockBlock());
	public static final RegistryObject<Block> MIINERIO_CRISTALIZADO_DOURADO_ORE = REGISTRY.register("miinerio_cristalizado_dourado_ore", () -> new Miinerio_Cristalizado_DouradoOreBlock());
	public static final RegistryObject<Block> MIINERIO_CRISTALIZADO_DOURADO_BLOCK = REGISTRY.register("miinerio_cristalizado_dourado_block", () -> new Miinerio_Cristalizado_DouradoBlockBlock());
	public static final RegistryObject<Block> MADEIRA_CLARA_ROSADA_WOOD = REGISTRY.register("madeira_clara_rosada_wood", () -> new MADEIRA_CLARA_ROSADAWoodBlock());
	public static final RegistryObject<Block> MADEIRA_CLARA_ROSADA_LOG = REGISTRY.register("madeira_clara_rosada_log", () -> new MADEIRA_CLARA_ROSADALogBlock());
	public static final RegistryObject<Block> MADEIRA_CLARA_ROSADA_PLANKS = REGISTRY.register("madeira_clara_rosada_planks", () -> new MADEIRA_CLARA_ROSADAPlanksBlock());
	public static final RegistryObject<Block> MADEIRA_CLARA_ROSADA_LEAVES = REGISTRY.register("madeira_clara_rosada_leaves", () -> new MADEIRA_CLARA_ROSADALeavesBlock());
	public static final RegistryObject<Block> MADEIRA_CLARA_ROSADA_STAIRS = REGISTRY.register("madeira_clara_rosada_stairs", () -> new MADEIRA_CLARA_ROSADAStairsBlock());
	public static final RegistryObject<Block> MADEIRA_CLARA_ROSADA_SLAB = REGISTRY.register("madeira_clara_rosada_slab", () -> new MADEIRA_CLARA_ROSADASlabBlock());
	public static final RegistryObject<Block> MADEIRA_CLARA_ROSADA_FENCE = REGISTRY.register("madeira_clara_rosada_fence", () -> new MADEIRA_CLARA_ROSADAFenceBlock());
	public static final RegistryObject<Block> MADEIRA_CLARA_ROSADA_FENCE_GATE = REGISTRY.register("madeira_clara_rosada_fence_gate", () -> new MADEIRA_CLARA_ROSADAFenceGateBlock());
	public static final RegistryObject<Block> MADEIRA_CLARA_ROSADA_PRESSURE_PLATE = REGISTRY.register("madeira_clara_rosada_pressure_plate", () -> new MADEIRA_CLARA_ROSADAPressurePlateBlock());
	public static final RegistryObject<Block> MADEIRA_CLARA_ROSADA_BUTTON = REGISTRY.register("madeira_clara_rosada_button", () -> new MADEIRA_CLARA_ROSADAButtonBlock());
	public static final RegistryObject<Block> MADEIRA_AZUL_FORTE_WOOD = REGISTRY.register("madeira_azul_forte_wood", () -> new MADEIRA_AZUL_FORTEWoodBlock());
	public static final RegistryObject<Block> MADEIRA_AZUL_FORTE_PLANKS = REGISTRY.register("madeira_azul_forte_planks", () -> new MADEIRA_AZUL_FORTEPlanksBlock());
	public static final RegistryObject<Block> MADEIRA_AZUL_FORTE_LOG = REGISTRY.register("madeira_azul_forte_log", () -> new MADEIRA_AZUL_FORTELogBlock());
	public static final RegistryObject<Block> MADEIRA_AZUL_FORTE_LEAVES = REGISTRY.register("madeira_azul_forte_leaves", () -> new MADEIRA_AZUL_FORTELeavesBlock());
	public static final RegistryObject<Block> MADEIRA_AZUL_FORTE_STAIRS = REGISTRY.register("madeira_azul_forte_stairs", () -> new MADEIRA_AZUL_FORTEStairsBlock());
	public static final RegistryObject<Block> MADEIRA_AZUL_FORTE_SLAB = REGISTRY.register("madeira_azul_forte_slab", () -> new MADEIRA_AZUL_FORTESlabBlock());
	public static final RegistryObject<Block> MADEIRA_AZUL_FORTE_FENCE = REGISTRY.register("madeira_azul_forte_fence", () -> new MADEIRA_AZUL_FORTEFenceBlock());
	public static final RegistryObject<Block> MADEIRA_AZUL_FORTE_FENCE_GATE = REGISTRY.register("madeira_azul_forte_fence_gate", () -> new MADEIRA_AZUL_FORTEFenceGateBlock());
	public static final RegistryObject<Block> MADEIRA_AZUL_FORTE_PRESSURE_PLATE = REGISTRY.register("madeira_azul_forte_pressure_plate", () -> new MADEIRA_AZUL_FORTEPressurePlateBlock());
	public static final RegistryObject<Block> MADEIRA_AZUL_FORTE_BUTTON = REGISTRY.register("madeira_azul_forte_button", () -> new MADEIRA_AZUL_FORTEButtonBlock());
	public static final RegistryObject<Block> MADEIRA_BRANCA_WOOD = REGISTRY.register("madeira_branca_wood", () -> new MADEIRA_BRANCAWoodBlock());
	public static final RegistryObject<Block> MADEIRA_BRANCA_LOG = REGISTRY.register("madeira_branca_log", () -> new MADEIRA_BRANCALogBlock());
	public static final RegistryObject<Block> MADEIRA_BRANCA_PLANKS = REGISTRY.register("madeira_branca_planks", () -> new MADEIRA_BRANCAPlanksBlock());
	public static final RegistryObject<Block> MADEIRA_BRANCA_LEAVES = REGISTRY.register("madeira_branca_leaves", () -> new MADEIRA_BRANCALeavesBlock());
	public static final RegistryObject<Block> MADEIRA_BRANCA_STAIRS = REGISTRY.register("madeira_branca_stairs", () -> new MADEIRA_BRANCAStairsBlock());
	public static final RegistryObject<Block> MADEIRA_BRANCA_SLAB = REGISTRY.register("madeira_branca_slab", () -> new MADEIRA_BRANCASlabBlock());
	public static final RegistryObject<Block> MADEIRA_BRANCA_FENCE = REGISTRY.register("madeira_branca_fence", () -> new MADEIRA_BRANCAFenceBlock());
	public static final RegistryObject<Block> MADEIRA_BRANCA_FENCE_GATE = REGISTRY.register("madeira_branca_fence_gate", () -> new MADEIRA_BRANCAFenceGateBlock());
	public static final RegistryObject<Block> MADEIRA_BRANCA_PRESSURE_PLATE = REGISTRY.register("madeira_branca_pressure_plate", () -> new MADEIRA_BRANCAPressurePlateBlock());
	public static final RegistryObject<Block> MADEIRA_BRANCA_BUTTON = REGISTRY.register("madeira_branca_button", () -> new MADEIRA_BRANCAButtonBlock());
}
