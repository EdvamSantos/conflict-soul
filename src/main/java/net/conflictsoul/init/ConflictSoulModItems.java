
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.conflictsoul.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.BlockItem;

import net.conflictsoul.item.Minerio_do_MineiroIngotItem;
import net.conflictsoul.item.MinerioSwordItem;
import net.conflictsoul.item.MinerioShovelItem;
import net.conflictsoul.item.MinerioPickaxeItem;
import net.conflictsoul.item.MinerioHoeItem;
import net.conflictsoul.item.MinerioDiamanteRosaItem;
import net.conflictsoul.item.MinerioDeCactoItem;
import net.conflictsoul.item.MinerioAxeItem;
import net.conflictsoul.item.Miinerio_Cristalizado_DouradoDustItem;
import net.conflictsoul.item.DIAMANTE_ROSA_KITSwordItem;
import net.conflictsoul.item.DIAMANTE_ROSA_KITShovelItem;
import net.conflictsoul.item.DIAMANTE_ROSA_KITPickaxeItem;
import net.conflictsoul.item.DIAMANTE_ROSA_KITHoeItem;
import net.conflictsoul.item.DIAMANTE_ROSA_KITAxeItem;
import net.conflictsoul.item.CristaliSwordItem;
import net.conflictsoul.item.CristaliShovelItem;
import net.conflictsoul.item.CristaliPickaxeItem;
import net.conflictsoul.item.CristaliHoeItem;
import net.conflictsoul.item.CristaliAxeItem;
import net.conflictsoul.item.ASwordItem;
import net.conflictsoul.item.AShovelItem;
import net.conflictsoul.item.APickaxeItem;
import net.conflictsoul.item.AHoeItem;
import net.conflictsoul.item.AAxeItem;
import net.conflictsoul.ConflictSoulMod;

public class ConflictSoulModItems {
	public static final DeferredRegister<Item> REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, ConflictSoulMod.MODID);
	public static final RegistryObject<Item> MINERIO_DO_MINEIRO_BLOCK = block(ConflictSoulModBlocks.MINERIO_DO_MINEIRO_BLOCK);
	public static final RegistryObject<Item> MINERIO_DO_MINEIRO_ORE = block(ConflictSoulModBlocks.MINERIO_DO_MINEIRO_ORE);
	public static final RegistryObject<Item> MINERIO_DO_MINEIRO_INGOT = REGISTRY.register("minerio_do_mineiro_ingot", () -> new Minerio_do_MineiroIngotItem());
	public static final RegistryObject<Item> MINERIO_DIAMANTE_ROSA_ORE = block(ConflictSoulModBlocks.MINERIO_DIAMANTE_ROSA_ORE);
	public static final RegistryObject<Item> MINERIO_DIAMANTE_ROSA_BLOCK = block(ConflictSoulModBlocks.MINERIO_DIAMANTE_ROSA_BLOCK);
	public static final RegistryObject<Item> MINERIO_DIAMANTE_ROSA = REGISTRY.register("minerio_diamante_rosa", () -> new MinerioDiamanteRosaItem());
	public static final RegistryObject<Item> MINERIO_DE_CACTO_ORE = block(ConflictSoulModBlocks.MINERIO_DE_CACTO_ORE);
	public static final RegistryObject<Item> MINERIO_DE_CACTO_BLOCK = block(ConflictSoulModBlocks.MINERIO_DE_CACTO_BLOCK);
	public static final RegistryObject<Item> MINERIO_DE_CACTO = REGISTRY.register("minerio_de_cacto", () -> new MinerioDeCactoItem());
	public static final RegistryObject<Item> MIINERIO_CRISTALIZADO_DOURADO_ORE = block(ConflictSoulModBlocks.MIINERIO_CRISTALIZADO_DOURADO_ORE);
	public static final RegistryObject<Item> MIINERIO_CRISTALIZADO_DOURADO_BLOCK = block(ConflictSoulModBlocks.MIINERIO_CRISTALIZADO_DOURADO_BLOCK);
	public static final RegistryObject<Item> MIINERIO_CRISTALIZADO_DOURADO_DUST = REGISTRY.register("miinerio_cristalizado_dourado_dust", () -> new Miinerio_Cristalizado_DouradoDustItem());
	public static final RegistryObject<Item> A_AXE = REGISTRY.register("a_axe", () -> new AAxeItem());
	public static final RegistryObject<Item> A_PICKAXE = REGISTRY.register("a_pickaxe", () -> new APickaxeItem());
	public static final RegistryObject<Item> A_SWORD = REGISTRY.register("a_sword", () -> new ASwordItem());
	public static final RegistryObject<Item> A_SHOVEL = REGISTRY.register("a_shovel", () -> new AShovelItem());
	public static final RegistryObject<Item> A_HOE = REGISTRY.register("a_hoe", () -> new AHoeItem());
	public static final RegistryObject<Item> DIAMANTE_ROSA_KIT_PICKAXE = REGISTRY.register("diamante_rosa_kit_pickaxe", () -> new DIAMANTE_ROSA_KITPickaxeItem());
	public static final RegistryObject<Item> DIAMANTE_ROSA_KIT_AXE = REGISTRY.register("diamante_rosa_kit_axe", () -> new DIAMANTE_ROSA_KITAxeItem());
	public static final RegistryObject<Item> DIAMANTE_ROSA_KIT_SWORD = REGISTRY.register("diamante_rosa_kit_sword", () -> new DIAMANTE_ROSA_KITSwordItem());
	public static final RegistryObject<Item> DIAMANTE_ROSA_KIT_SHOVEL = REGISTRY.register("diamante_rosa_kit_shovel", () -> new DIAMANTE_ROSA_KITShovelItem());
	public static final RegistryObject<Item> DIAMANTE_ROSA_KIT_HOE = REGISTRY.register("diamante_rosa_kit_hoe", () -> new DIAMANTE_ROSA_KITHoeItem());
	public static final RegistryObject<Item> MINERIO_PICKAXE = REGISTRY.register("minerio_pickaxe", () -> new MinerioPickaxeItem());
	public static final RegistryObject<Item> MINERIO_AXE = REGISTRY.register("minerio_axe", () -> new MinerioAxeItem());
	public static final RegistryObject<Item> MINERIO_SWORD = REGISTRY.register("minerio_sword", () -> new MinerioSwordItem());
	public static final RegistryObject<Item> MINERIO_SHOVEL = REGISTRY.register("minerio_shovel", () -> new MinerioShovelItem());
	public static final RegistryObject<Item> MINERIO_HOE = REGISTRY.register("minerio_hoe", () -> new MinerioHoeItem());
	public static final RegistryObject<Item> CRISTALI_PICKAXE = REGISTRY.register("cristali_pickaxe", () -> new CristaliPickaxeItem());
	public static final RegistryObject<Item> CRISTALI_AXE = REGISTRY.register("cristali_axe", () -> new CristaliAxeItem());
	public static final RegistryObject<Item> CRISTALI_SWORD = REGISTRY.register("cristali_sword", () -> new CristaliSwordItem());
	public static final RegistryObject<Item> CRISTALI_SHOVEL = REGISTRY.register("cristali_shovel", () -> new CristaliShovelItem());
	public static final RegistryObject<Item> CRISTALI_HOE = REGISTRY.register("cristali_hoe", () -> new CristaliHoeItem());
	public static final RegistryObject<Item> MADEIRA_CLARA_ROSADA_WOOD = block(ConflictSoulModBlocks.MADEIRA_CLARA_ROSADA_WOOD);
	public static final RegistryObject<Item> MADEIRA_CLARA_ROSADA_LOG = block(ConflictSoulModBlocks.MADEIRA_CLARA_ROSADA_LOG);
	public static final RegistryObject<Item> MADEIRA_CLARA_ROSADA_PLANKS = block(ConflictSoulModBlocks.MADEIRA_CLARA_ROSADA_PLANKS);
	public static final RegistryObject<Item> MADEIRA_CLARA_ROSADA_LEAVES = block(ConflictSoulModBlocks.MADEIRA_CLARA_ROSADA_LEAVES);
	public static final RegistryObject<Item> MADEIRA_CLARA_ROSADA_STAIRS = block(ConflictSoulModBlocks.MADEIRA_CLARA_ROSADA_STAIRS);
	public static final RegistryObject<Item> MADEIRA_CLARA_ROSADA_SLAB = block(ConflictSoulModBlocks.MADEIRA_CLARA_ROSADA_SLAB);
	public static final RegistryObject<Item> MADEIRA_CLARA_ROSADA_FENCE = block(ConflictSoulModBlocks.MADEIRA_CLARA_ROSADA_FENCE);
	public static final RegistryObject<Item> MADEIRA_CLARA_ROSADA_FENCE_GATE = block(ConflictSoulModBlocks.MADEIRA_CLARA_ROSADA_FENCE_GATE);
	public static final RegistryObject<Item> MADEIRA_CLARA_ROSADA_PRESSURE_PLATE = block(ConflictSoulModBlocks.MADEIRA_CLARA_ROSADA_PRESSURE_PLATE);
	public static final RegistryObject<Item> MADEIRA_CLARA_ROSADA_BUTTON = block(ConflictSoulModBlocks.MADEIRA_CLARA_ROSADA_BUTTON);
	public static final RegistryObject<Item> MADEIRA_AZUL_FORTE_WOOD = block(ConflictSoulModBlocks.MADEIRA_AZUL_FORTE_WOOD);
	public static final RegistryObject<Item> MADEIRA_AZUL_FORTE_PLANKS = block(ConflictSoulModBlocks.MADEIRA_AZUL_FORTE_PLANKS);
	public static final RegistryObject<Item> MADEIRA_AZUL_FORTE_LOG = block(ConflictSoulModBlocks.MADEIRA_AZUL_FORTE_LOG);
	public static final RegistryObject<Item> MADEIRA_AZUL_FORTE_LEAVES = block(ConflictSoulModBlocks.MADEIRA_AZUL_FORTE_LEAVES);
	public static final RegistryObject<Item> MADEIRA_AZUL_FORTE_STAIRS = block(ConflictSoulModBlocks.MADEIRA_AZUL_FORTE_STAIRS);
	public static final RegistryObject<Item> MADEIRA_AZUL_FORTE_SLAB = block(ConflictSoulModBlocks.MADEIRA_AZUL_FORTE_SLAB);
	public static final RegistryObject<Item> MADEIRA_AZUL_FORTE_FENCE = block(ConflictSoulModBlocks.MADEIRA_AZUL_FORTE_FENCE);
	public static final RegistryObject<Item> MADEIRA_AZUL_FORTE_FENCE_GATE = block(ConflictSoulModBlocks.MADEIRA_AZUL_FORTE_FENCE_GATE);
	public static final RegistryObject<Item> MADEIRA_AZUL_FORTE_PRESSURE_PLATE = block(ConflictSoulModBlocks.MADEIRA_AZUL_FORTE_PRESSURE_PLATE);
	public static final RegistryObject<Item> MADEIRA_AZUL_FORTE_BUTTON = block(ConflictSoulModBlocks.MADEIRA_AZUL_FORTE_BUTTON);
	public static final RegistryObject<Item> MADEIRA_BRANCA_WOOD = block(ConflictSoulModBlocks.MADEIRA_BRANCA_WOOD);
	public static final RegistryObject<Item> MADEIRA_BRANCA_LOG = block(ConflictSoulModBlocks.MADEIRA_BRANCA_LOG);
	public static final RegistryObject<Item> MADEIRA_BRANCA_PLANKS = block(ConflictSoulModBlocks.MADEIRA_BRANCA_PLANKS);
	public static final RegistryObject<Item> MADEIRA_BRANCA_LEAVES = block(ConflictSoulModBlocks.MADEIRA_BRANCA_LEAVES);
	public static final RegistryObject<Item> MADEIRA_BRANCA_STAIRS = block(ConflictSoulModBlocks.MADEIRA_BRANCA_STAIRS);
	public static final RegistryObject<Item> MADEIRA_BRANCA_SLAB = block(ConflictSoulModBlocks.MADEIRA_BRANCA_SLAB);
	public static final RegistryObject<Item> MADEIRA_BRANCA_FENCE = block(ConflictSoulModBlocks.MADEIRA_BRANCA_FENCE);
	public static final RegistryObject<Item> MADEIRA_BRANCA_FENCE_GATE = block(ConflictSoulModBlocks.MADEIRA_BRANCA_FENCE_GATE);
	public static final RegistryObject<Item> MADEIRA_BRANCA_PRESSURE_PLATE = block(ConflictSoulModBlocks.MADEIRA_BRANCA_PRESSURE_PLATE);
	public static final RegistryObject<Item> MADEIRA_BRANCA_BUTTON = block(ConflictSoulModBlocks.MADEIRA_BRANCA_BUTTON);

	private static RegistryObject<Item> block(RegistryObject<Block> block) {
		return REGISTRY.register(block.getId().getPath(), () -> new BlockItem(block.get(), new Item.Properties()));
	}
}
